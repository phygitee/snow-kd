import com.ruoyi.generator.config.GenConfig;
import com.ruoyi.generator.domain.GenTable;
import com.ruoyi.generator.service.impl.GenTableServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

/**
 * @author kdyzm
 */
public class Main {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(GenConfig.class);

        ctx.refresh();

        GenManager bean = ctx.getBean(GenManager.class);
        bean.generate("dept","var");
        System.out.print("生成结束，ruoyi.zip为生成文件");
    }
}
