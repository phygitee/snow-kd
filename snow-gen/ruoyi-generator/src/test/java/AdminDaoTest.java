import com.ruoyi.generator.mapper.GenTableColumnMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.sql.Connection;
import java.util.Date;

@TestPropertySource("classpath:application-dao.properties")
@RunWith(SpringRunner.class)
@ComponentScan(basePackages = "com.ruoyi.generator")
@SpringBootTest(classes = AdminDaoTest.class)
@EnableAutoConfiguration
class AdminDaoTest {

    @Resource
    private GenTableColumnMapper genTableColumnMapper;


    @Test
    void selectByUsername() {
        System.out.println(genTableColumnMapper.selectDbTableColumnsByName("admin"));
    }

    @Test
    void insert() {
    }
}